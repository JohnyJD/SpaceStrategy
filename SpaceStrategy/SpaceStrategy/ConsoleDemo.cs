﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game;
using SpaceStrategy.PlayerObjects;
using SpaceStrategy.PlayerObjects.Species;
using Console = System.Console;

namespace SpaceStrategy
{
    public class ConsoleDemo
    {
        public void clear()
        {
            Console.Clear();
        }

        public string init()
        {
            clear();
            Console.WriteLine("Spustenie apky");
            Console.WriteLine("Zadajte meno hraca: ");
            string result = Console.ReadLine();
            return result ?? "";
        }

        public string? menu()
        {
            clear();
            Console.WriteLine("Menu");
            Console.WriteLine("1 - Vytvor galaxiu");
            Console.WriteLine("2 - Vybrat si hernu rasu");
            Console.WriteLine("3 - Pridaj hracov");
            Console.WriteLine("4 - Spust hru");
            Console.WriteLine("5 - Koniec");
            string result = Console.ReadLine();
            return result ?? "";
        }

        public Player[] createPlayers()
        {
            bool valid = false;
            string error = "";
            string result = "";
            while (!valid)
            {
                clear();
                Console.WriteLine(error);
                Console.WriteLine("Kolko hracov : 1 - 4");
                result = Console.ReadLine();
                valid = validate(result, ref error);
            }
            int playerCount = Int32.Parse(result);
            Player[] players = new Player[playerCount];
            for (int i = 0; i < playerCount; i++)
            {
                Player player = new AIPlayer($"Player {i}");

                players[i] = player;

            }

            return players;
        }

        public AlienRace chooseRace()
        {
            AlienRace race = null;
            bool valid = false;
            string error = "";
            string result = "";
            while (!valid)
            {
                clear();
                Console.WriteLine(error);
                Console.WriteLine("Vyber si rasu");
                Console.WriteLine("1 - Avian");
                Console.WriteLine("2 - Humanoid");
                Console.WriteLine("3 - Reptilian");
                result = Console.ReadLine();
                valid = validate(result, ref error);
            }
            
            switch (result)
            {
                case "1":
                    race = new Avian();
                    break;
                case "2":
                    race = new Humanoid();
                    break;
                case "3":
                    race = new Reptilian();
                    break;
            }

            return race;
        }

        private static bool validate(string count, ref string message)
        {
            try
            {
                Int32.Parse(count);
                message = "";
                return true;
            }
            catch (Exception e)
            {
                message = "Nespravna hodnota";
                return false;
            }
        }

        public GalaxySize chooseGalaxySize()
        {
            GalaxySize size = GalaxySize.SMALL;
            bool valid = false;
            string error = "";
            string result = "";
            while (!valid)
            {
                clear();
                Console.WriteLine(error);
                Console.WriteLine("Vyber si velkost mapy");
                Console.WriteLine("1 - Mala");
                Console.WriteLine("2 - Stredna");
                Console.WriteLine("3 - Velka");
                result = Console.ReadLine();
                valid = validate(result, ref error);
            }

            switch (result)
            {
                case "1":
                    size = GalaxySize.SMALL;
                    break;
                case "2":
                    size = GalaxySize.MEDIUM;
                    break;
                case "3":
                    size = GalaxySize.LARGE;
                    break;
            }

            return size;
        }
    }
}
