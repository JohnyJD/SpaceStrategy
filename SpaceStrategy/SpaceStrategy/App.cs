﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game;
using SpaceStrategy.PlayerObjects;

namespace SpaceStrategy
{
    public sealed class App
    {
        private static App _instance = null;
        public GameController GameController {get; private set; }

        private App()
        {
            GameController = new GameController();
        }

        public static App GetInstance()
        {
            if (_instance == null)
            {
                _instance = new App();
            }
            return _instance;
        }

        public void createGame()
        {
            GameController.createGame();
        }

        public void startGame()
        {
            GameController.startGame();
        }

        public void endGame()
        {
            GameController.endGame();
        }

        public void addPlayer(Player player)
        {
            GameController.addPlayer(player);
        }

        public void init(Player me)
        {
            GameController.addPlayer(me);
        }

        public Player? getPlayer(string name)
        {
            return GameController.getPlayer(name);
        }

        public bool isGameReady()
        {
            return GameController.isGameReady();
        }
    }
}
