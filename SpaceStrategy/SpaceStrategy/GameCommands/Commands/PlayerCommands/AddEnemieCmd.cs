﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.PlayerObjects;

namespace SpaceStrategy.GameCommands.Commands.PlayerCommands
{
    public class AddEnemieCmd : PlayerCommand
    {
        public AddEnemieCmd(Player me, Player player)
        {
            Me = me;
            Player = player;
        }
        public override void execute()
        {
            addEnemie();
        }

        private void addEnemie()
        {
            Me.addEnemie(Player);
        }
    }
}
