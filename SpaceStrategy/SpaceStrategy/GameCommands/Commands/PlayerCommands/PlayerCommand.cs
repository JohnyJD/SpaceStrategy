﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.PlayerObjects;

namespace SpaceStrategy.GameCommands.Commands.PlayerCommands
{
    public abstract class PlayerCommand : ControlCommand
    {
        public Player Me { get; set; }
        public Player Player { get; set; }


        public abstract void execute();
    }
}
