﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.PlayerObjects;

namespace SpaceStrategy.GameCommands.Commands.PlayerCommands
{
    public class AddAllieCmd : PlayerCommand
    {
        public AddAllieCmd(Player me, Player player)
        {
            Me = me;
            Player = player;
        }
        public override void execute()
        {
            addAllie();
        }

        private void addAllie()
        {
            Me.addAllie(Player);
        }
    }
}
