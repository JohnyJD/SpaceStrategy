﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard;
using SpaceStrategy.PlayerObjects;

namespace SpaceStrategy.GameCommands.Commands.SectorCommands
{
    public class ReleaseCommand : SectorCommand
    {
        public ReleaseCommand(Player player, Sector sector)
        {
            Me = player;
            Sector = sector;
        }
        public override void execute()
        {
            release();
        }

        private void release()
        {
            Me.releaseSector(Sector);
        }
    }
}
