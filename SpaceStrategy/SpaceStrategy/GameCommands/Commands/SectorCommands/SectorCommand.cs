﻿using SpaceStrategy.PlayerObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard;

namespace SpaceStrategy.GameCommands.Commands.SectorCommands
{
    public abstract class SectorCommand : ControlCommand
    {
        public Player Me { get; set; }
        public Sector Sector { get; set; }


        public abstract void execute();
    }
}
