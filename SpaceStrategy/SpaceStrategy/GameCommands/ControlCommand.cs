﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.GameCommands
{
    public interface ControlCommand
    {
        public void execute();
    }
}
