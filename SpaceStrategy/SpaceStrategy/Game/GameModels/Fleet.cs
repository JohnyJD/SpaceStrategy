﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameModels.Ships;

namespace SpaceStrategy.Game.GameModels
{
    public abstract class Fleet
    {
        public string Id { get; set; }
        public LinkedList<Ship> Ships { get; set; }

        protected Fleet()
        {
            Ships = new LinkedList<Ship>();
        }

        public abstract void addShip(Ship ship);
        public abstract void removeShip(Ship ship);
    }
}
