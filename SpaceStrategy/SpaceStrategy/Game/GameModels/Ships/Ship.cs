﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameModels.Ships
{
    public abstract class Ship
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public int Health { get; set; }
        public float Speed { get; set; }
    }
}
