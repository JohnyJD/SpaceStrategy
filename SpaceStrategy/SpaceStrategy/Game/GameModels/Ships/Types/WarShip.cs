﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameModels.Ships.Types
{
    public abstract class WarShip : Ship
    {
        public int Power { get; set; }
    }
}
