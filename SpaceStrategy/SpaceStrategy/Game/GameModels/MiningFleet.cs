﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameModels.Ships;
using SpaceStrategy.Game.GameModels.Ships.Types;

namespace SpaceStrategy.Game.GameModels
{
    public class MiningFleet : Fleet
    {
        public override void addShip(Ship ship)
        {
            if (ship.GetType() == typeof(MiningShip))
            {
                Ships.AddLast(ship);
            }
        }

        public override void removeShip(Ship ship)
        {
            if (ship.GetType() == typeof(MiningShip))
            {
                Ships.Remove(ship);
            }
        }
    }
}
