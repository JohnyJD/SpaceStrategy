﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game
{
    public enum GalaxySize
    {
        SMALL, MEDIUM, LARGE
    }
}
