﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.PlayerObjects;

namespace SpaceStrategy.Game
{
    public class GameController
    {
        public Game Game { get; private set; }

        public GameController()
        {
            Game = new Game();
        }   

        public void createGame()
        {
            Game.createGame();
        }

        public void startGame()
        {
            Game.startGame();
        }

        public void endGame()
        {
            Game.endGame();
        }

        public void addPlayer(PlayerObjects.Player player)
        {
            Game.addPlayer(player);
        }

        public void evaluateGame()
        {
            Game.evaluateGame();
        }

        public Player? getPlayer(string name)
        {
            return Game.Players.First(x => x.PlayerName == name);
        }

        public bool isGameReady()
        {
            return Game.GameBoard != null && Game.Players.Count > 0;
        }
    }
}
