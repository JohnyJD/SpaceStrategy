﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard;
using SpaceStrategy.Game.GameBoard.Sectors;

namespace SpaceStrategy.Game.Galaxies
{
    public class BasicGalaxy : Galaxy
    {
        public Sector[,] Sectors { get; set; }
        public int SectorsX { get; set; }
        public int SectorsY { get; set; }
        public BasicGalaxy()
        {
            GetAxis();
            Sectors = new Sector[SectorsX, SectorsY];
        }

        private void GetAxis()
        {
            GalaxySize size = GameSettings.getInstance().GalaxySize;
            switch (size)
            {
                case GalaxySize.SMALL:
                    SectorsX = 4;
                    SectorsY = 4;
                    break;
                case GalaxySize.MEDIUM:
                    SectorsX = 5;
                    SectorsY = 5;
                    break;
                case GalaxySize.LARGE:
                    SectorsX = 6;
                    SectorsY = 6;
                    break;
                default:
                    SectorsX = 0;
                    SectorsY = 0;
                    break;
            }
        }

        public override List<Sector>? getPossibleNeighbors(Sector? sector)
        {
            if (sector == null)
            {
                return null;
            }
            int x = sector.Pos_X;
            int y = sector.Pos_Y;
            int max_x = SectorsX;
            int max_y = SectorsY;

            if ((x < 0 || x >= max_x) || (y < 0 || y >= max_y))
            {
                return null;
            }

            List<Sector> neighbors = new List<Sector>();

            if (y + 1 < max_y)
            {
                neighbors.Add(Sectors[x, y + 1]);
            }   

            if (y - 1 >= 0)
            {
                neighbors.Add(Sectors[x, y - 1]);
            }
            if (x + 1 < max_x)
            {
                neighbors.Add(Sectors[x + 1, y]);
            }

            if (x - 1 >= 0)
            {
                neighbors.Add(Sectors[x - 1, y]);
            }

            return neighbors;
        }

        public override string toString()
        {
            int planetary_s = 0;
            int asteroid_s = 0;
            int void_s = 0;
            string text = "";
            for (int i = 0; i < SectorsX; i++)
            {
                for (int j = 0; j < SectorsY; j++)
                {
                    text += "| " + Sectors[i, j].toString() + $" ({i},{j}) |";
                    if (Sectors[i, j].GetType() == typeof(PlanetarySector))
                    {
                        planetary_s++;
                    }
                    if (Sectors[i, j].GetType() == typeof(AsteroidSector))
                    {
                        asteroid_s++;
                    }
                    if (Sectors[i, j].GetType() == typeof(VoidSector))
                    {
                        void_s++;
                    }
                }
                text += "\n";
            }
            text += $"-----------------------------------------------------------\n";
            text += $"Planetary : {planetary_s}\n";
            text += $"Asteroid : {asteroid_s}\n";
            text += $"Void : {void_s}\n";
            text += $"Total : {Sectors.Length}\n";
            return text;
        }

        public override List<Sector> GetSectors()
        {
            List<Sector> sectorsList = new List<Sector>();
            foreach (Sector sector in Sectors)
            {
                sectorsList.Add(sector);
            }
            return sectorsList;
        }
    }
}
