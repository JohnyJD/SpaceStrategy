﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard.SpaceSystems;

namespace SpaceStrategy.Game.GameBoard.Sectors
{
    public class AsteroidSector : Sector
    {
        public override void createSystem()
        {
            System = new AsteroidSystem();
        }

        public override string toString()
        {
            return "A.S.";
        }
    }
}
