﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard.SpaceSystems;

namespace SpaceStrategy.Game.GameBoard.Sectors
{
    public class PlanetarySector : Sector
    {
        public override void createSystem()
        {
            System = new PlanetarySystem();
        }

        public override string toString()
        {
            return "P.S.";
        }
    }
}
