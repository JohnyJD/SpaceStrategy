﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard.Sectors
{
    public class VoidSector : Sector
    {
        public override void createSystem()
        {
            System = null;
        }

        public override string toString()
        {
            return "V.S.";
        }
    }
}
