﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.GameObserver;
using SpaceStrategy.PlayerObjects;

namespace SpaceStrategy.Game.GameBoard
{
    public abstract class Sector : Observable
    {
        public string Id { get; set; } = "";
        public int Pos_X { get; set; } = -1;
        public int Pos_Y { get; set; } = -1;
        public LinkedList<Sector> NearSectors { get; set; }
        public LinkedList<Player> Players { get; set; }
        public Player Occupant { get; set; } = null;
        public LinkedList<Observer> Observers { get; set; }
        public SpaceSystem System { get; set; } = null;

        protected Sector()
        {
            NearSectors = new LinkedList<Sector>();
            Observers = new LinkedList<Observer>();
            Players = new LinkedList<Player>();
        }

        public void attach(Observer observer)
        {
            Observers.AddLast(observer);
        }

        public void detach(Observer observer)
        {
            Observers.Remove(observer);
        }

        public void notify()
        {
            foreach (Observer observer in Observers)
            {
                observer.update(this);
            }
        }

        public void addNeighbor(Sector neighbor)
        {
            var aa = NearSectors.ToList()
                .Find(sector => sector.Pos_X == neighbor.Pos_X && sector.Pos_Y == neighbor.Pos_Y);
            if (NearSectors.Count == 0 || NearSectors.ToList().Find(sector => sector.Pos_X == neighbor.Pos_X && sector.Pos_Y == neighbor.Pos_Y) == null)
            {
                NearSectors.AddLast(neighbor);
            }
        }

        public abstract void createSystem();

        public abstract string toString();

    }
}
