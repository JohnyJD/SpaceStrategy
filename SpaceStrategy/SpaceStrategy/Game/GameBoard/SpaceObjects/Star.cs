﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard.SpaceObjects
{
    public class Star : SpaceObject
    {
        public int Energy { get; set; }

        public Star()
        {
            Energy = 100;   // 100GW of energy by initialization
        }
    }
}
