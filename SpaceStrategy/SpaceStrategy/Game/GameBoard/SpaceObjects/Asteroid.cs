﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard.SpaceObjects
{
    public enum Rarity
    {
        COMMON, RARE, VERYRARE
    }
    public class Asteroid : SpaceObject 
    {
        public Rarity Rarity { get; set; }
    }
}
