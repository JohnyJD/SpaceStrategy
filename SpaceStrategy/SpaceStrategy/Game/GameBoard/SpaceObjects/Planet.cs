﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard.SpaceObjects
{
    public class Planet : SpaceObject
    {
        public PlayerObjects.Player Player { get; set; }
        public int Population { get; set; }
        public bool CanHostLife { get; set; }

        public Planet()
        {
            
        }
    }
}
