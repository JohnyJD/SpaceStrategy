﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard.GalaxyFactories
{
    public interface IGalaxyFactory
    {
        public Galaxy createGalaxy();
    }
}
