﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.Galaxies;

namespace SpaceStrategy.Game.GameBoard.GalaxyFactories.Factories
{
    public class BasicGalaxyFactory : GalaxyFactory
    {
        protected override Galaxy buildGalaxy()
        {
            return new BasicGalaxy();
        }

        protected override void buildSectors(Galaxy galaxy)
        {
            BasicGalaxy basicGalaxy = ((BasicGalaxy)galaxy);
            int row = -1;
            for (int i = 0; i < basicGalaxy.Sectors.Length; i++)
            {
                Sector sector = createSector();
                sector.Id = i.ToString();
                int col = i % basicGalaxy.SectorsY;
                if (col == 0)
                {
                    row++;
                }
                sector.Pos_X = row;
                sector.Pos_Y = col;
                basicGalaxy.Sectors[row, col] = sector;
            }

            foreach (Sector sector in basicGalaxy.Sectors)
            {
                List<Sector> possibleNeighbors = galaxy.getPossibleNeighbors(sector);
                foreach (Sector neighbor in possibleNeighbors)
                {
                    sector.addNeighbor(neighbor);
                    neighbor.addNeighbor(sector);
                }
            }
        }

        protected override void buildSpaceSystems(Galaxy galaxy)
        {
            BasicGalaxy basicGalaxy = ((BasicGalaxy)galaxy);
            foreach (Sector sector in basicGalaxy.Sectors)
            {
                sector.createSystem();
            }
        }
    }
}
