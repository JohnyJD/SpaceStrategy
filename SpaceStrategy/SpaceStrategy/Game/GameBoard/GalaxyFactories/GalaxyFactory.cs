﻿using SpaceStrategy.Game.GameBoard.Sectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard.GalaxyFactories
{
    public abstract class GalaxyFactory : IGalaxyFactory
    {
        public Random Random { get; set; }

        protected GalaxyFactory()
        {
            Random = new Random(DateTime.Now.Millisecond);
        }
        public Galaxy createGalaxy()
        {
            Galaxy galaxy = buildGalaxy();
            buildSectors(galaxy);
            buildSpaceSystems(galaxy);
            return galaxy;
        }
        protected abstract Galaxy buildGalaxy();
        protected abstract void buildSectors(Galaxy galaxy);
        protected abstract void buildSpaceSystems(Galaxy galaxy);

        protected Sector createSector()
        {
            
            GameSettings settings = GameSettings.getInstance();

            double random = Random.NextDouble();
            Sector sector;
            if (settings.AsteroidSystemRatio > random)
            {
                sector = new AsteroidSector();
            }
            else if (settings.AsteroidSystemRatio <= random &&
                     settings.PlanetarySystemRatio + settings.AsteroidSystemRatio > random)
            {
                sector = new PlanetarySector();
            }
            else
            {
                sector = new VoidSector();
            }
            return sector;
        }
    }
}
