﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard.SpaceObjects;

namespace SpaceStrategy.Game.GameBoard.SpaceSystems
{
    public class AsteroidSystem : SpaceSystem
    {
        public Asteroid Base { get; set; }

        public AsteroidSystem()
        {
            Base = new Asteroid();
        }
    }
}
