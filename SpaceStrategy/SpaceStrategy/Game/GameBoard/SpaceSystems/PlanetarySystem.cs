﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard.SpaceObjects;

namespace SpaceStrategy.Game.GameBoard.SpaceSystems
{
    public class PlanetarySystem : SpaceSystem
    {
        public Star Star { get; set; }

        public PlanetarySystem() : base()
        {
            Star = new Star();
        }
    }
}
