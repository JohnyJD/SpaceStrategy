﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard
{
    public enum ObjectSize
    {
        SMALL,MEDIUM,LARGE
    }
    public class SpaceObject
    {
        public string Id { get; set; }
        public ObjectSize Size { get; set; }

        public SpaceObject()
        {
            
        }
    }
}
