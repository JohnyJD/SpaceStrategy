﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard
{
    public abstract class Galaxy
    {
        public abstract List<Sector>? getPossibleNeighbors(Sector? sector);
        public abstract string toString();
        public abstract List<Sector> GetSectors();
    }
}
