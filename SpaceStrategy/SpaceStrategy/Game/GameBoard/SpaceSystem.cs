﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game.GameBoard
{
    public abstract class SpaceSystem
    {
        public IEnumerable<SpaceObject> SpaceObjects { get; set; }

        protected SpaceSystem()
        {
            SpaceObjects = new LinkedList<SpaceObject>();
        }
    }
}
