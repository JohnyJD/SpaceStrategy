﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.Game
{
    public sealed class GameSettings
    {
        public static GameSettings _instance = null;
        public GalaxySize GalaxySize { get; set; }
        public float PlanetarySystemRatio { get; set; }
        public float AsteroidSystemRatio { get; set; }
        public float ResourceMultiplier { get; set; }
        private GameSettings()
        {
            setDefaults();
        }

        private void setDefaults()
        {
            GalaxySize = GalaxySize.LARGE;
            PlanetarySystemRatio = 0.3f;
            AsteroidSystemRatio = 0.6f;
            ResourceMultiplier = 1.0f;
        }

        public static GameSettings getInstance()
        {
            if (_instance == null)
            {
                _instance = new GameSettings();
            }

            return _instance;
        }

        public int getSectorCount()
        {
            switch (GalaxySize)
            {
                case GalaxySize.SMALL:
                    return 16;
                case GalaxySize.MEDIUM:
                    return 25;
                case GalaxySize.LARGE:
                    return 36;
                default:
                    return 0;
            }
        }
    }
}
