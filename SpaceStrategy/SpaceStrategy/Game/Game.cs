﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard;
using SpaceStrategy.Game.GameBoard.GalaxyFactories;
using SpaceStrategy.Game.GameBoard.GalaxyFactories.Factories;
using SpaceStrategy.GameObserver;

namespace SpaceStrategy.Game
{
    public class Game : Observer, Observable
    {
        public LinkedList<PlayerObjects.Player> Players { get; set; }
        public Galaxy GameBoard { get; set; } = null;
        public IGalaxyFactory GalaxyFactory { get; set; }
        public bool isRunning { get; set; } = false;
        public LinkedList<Observer> Observers { get; set; }

        public Game()
        {
            Players = new LinkedList<PlayerObjects.Player>();
            GalaxyFactory = new BasicGalaxyFactory();
            Observers = new LinkedList<Observer>();
        }

        public void createGame()
        {
            GameBoard = GalaxyFactory.createGalaxy();
            foreach (Sector sector in GameBoard.GetSectors())
            {
                sector.attach(this);
            }
        }

        public void startGame()
        {
            isRunning = true;
        }

        public void endGame()
        {
            isRunning = false;
            ResetGameBoard();
        }

        private void ResetGameBoard()
        {
            createGame();
        }

        public void addPlayer(PlayerObjects.Player player)
        {
            Players.AddLast(player);
            Observers.AddLast(player);
        }
        
        public void evaluateGame()
        {
            throw new NotImplementedException();
        }

        public void update(Observable observable)
        {
            if (observable.GetType() != typeof(Game))
            {
                notify();
            }
        }

        public void update(Observable observable, object param)
        {
            if (observable.GetType() != typeof(Game))
            {
                notify();
            }
        }

        public void attach(Observer observer)
        {
            Observers.AddLast(observer);
        }

        public void detach(Observer observer)
        {
            Observers.Remove(observer);
        }

        public void notify()
        {
            foreach (Observer observer in Observers)
            {
                observer.update(this);
            }
        }
    }
}
