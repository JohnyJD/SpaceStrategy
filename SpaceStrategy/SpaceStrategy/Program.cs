﻿using SpaceStrategy;
using SpaceStrategy.Game.GameBoard;
using SpaceStrategy.GameCommands.Commands.SectorCommands;
using SpaceStrategy.PlayerObjects;

AppDemo demo = new AppDemo();
demo.init();
demo.start();