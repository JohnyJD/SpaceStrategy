﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameBoard;
using SpaceStrategy.GameObserver;

namespace SpaceStrategy.PlayerObjects
{
    public class Player : Observer
    {
        public string PlayerName { get; set; } = "";
        public bool IsHuman { get; set; } = false;
        public PlayerStats Stats { get; set; }
        public AlienRace Race { get; set; } = null;
        public Sector CurrentSector { get; set; } = null;
        
        public Player()
        {
            Stats = new PlayerStats();
        }

        public Player(string name)
        {
            Stats = new PlayerStats();
            PlayerName = name;
        }

        public void captureSector(Sector sector)
        {
            Stats.CapturedSectors.AddLast(sector);
            sector.Occupant = this;
        }

        public void releaseSector(Sector sector)
        {
            Stats.CapturedSectors.Remove(sector);
            sector.Occupant = null;
        }

        public void addRace(AlienRace race)
        {
            Race = race;
        }

        public void update(Observable observable)
        {
            Console.WriteLine($"{PlayerName} : Something has happened");
        }

        public void update(Observable observable, object param)
        {
            throw new NotImplementedException();
        }

        public void addEnemie(Player player)
        {
            Stats.Enemies.AddLast(player);
        }

        public void addAllie(Player player)
        {
            Stats.Allies.AddLast(player);
        }
    }
}
