﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameModels.Ships;
using SpaceStrategy.Game.GameModels.Ships.Types;
using SpaceStrategy.PlayerObjects.Species.ModelsFactory;

namespace SpaceStrategy.PlayerObjects
{
    public abstract class AlienRace
    {
        public IAlienRaceFactory RaceFactory { get; set; }
        protected AlienRace(IAlienRaceFactory raceFactory)
        {
            RaceFactory = raceFactory;
        }

        public WarShip getNewWarShip()
        {
            return RaceFactory.createWarShip();
        }

        public MiningShip getNewMiningShip()
        {
            return RaceFactory.createMiningShip();
        }
    }
}
