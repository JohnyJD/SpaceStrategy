﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameModels.Ships;
using SpaceStrategy.Game.GameModels.Ships.Races;
using SpaceStrategy.Game.GameModels.Ships.Races.Avian;
using SpaceStrategy.Game.GameModels.Ships.Types;

namespace SpaceStrategy.PlayerObjects.Species.ModelsFactory.Factories
{
    public class AvianFactory : IAlienRaceFactory
    {
        public AvianFactory()
        {
            
        }

        public WarShip createWarShip()
        {
            return new AvianWarShip();
        }

        public MiningShip createMiningShip()
        {
            return new AvianMiningShip();
        }
    }
}
