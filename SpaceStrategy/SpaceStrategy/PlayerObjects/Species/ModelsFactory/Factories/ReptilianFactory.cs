﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameModels.Ships;
using SpaceStrategy.Game.GameModels.Ships.Races;
using SpaceStrategy.Game.GameModels.Ships.Races.Reptilian;
using SpaceStrategy.Game.GameModels.Ships.Types;

namespace SpaceStrategy.PlayerObjects.Species.ModelsFactory.Factories
{
    public class ReptilianFactory : IAlienRaceFactory
    {
        public ReptilianFactory()
        {
            
        }

        public WarShip createWarShip()
        {
            return new ReptilianWarShip();
        }

        public MiningShip createMiningShip()
        {
            return new ReptilianMiningShip();
        }
    }
}
