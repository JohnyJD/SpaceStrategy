﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameModels.Ships;
using SpaceStrategy.Game.GameModels.Ships.Races;
using SpaceStrategy.Game.GameModels.Ships.Races.Humanoid;
using SpaceStrategy.Game.GameModels.Ships.Types;

namespace SpaceStrategy.PlayerObjects.Species.ModelsFactory.Factories
{
    public class HumanoidFactory : IAlienRaceFactory
    {
        public HumanoidFactory()
        {
            
        }

        public WarShip createWarShip()
        {
            return new HumanoidWarShip();
        }

        public MiningShip createMiningShip()
        {
            return new HumanoidMiningShip();
        }
    }
}
