﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameModels.Ships;
using SpaceStrategy.Game.GameModels.Ships.Types;

namespace SpaceStrategy.PlayerObjects.Species.ModelsFactory
{
    public interface IAlienRaceFactory
    {
        public WarShip createWarShip();
        public MiningShip createMiningShip();
    }
}
