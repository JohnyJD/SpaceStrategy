﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.PlayerObjects.Species.ModelsFactory;
using SpaceStrategy.PlayerObjects.Species.ModelsFactory.Factories;

namespace SpaceStrategy.PlayerObjects.Species
{
    public class Reptilian : AlienRace
    {
        public Reptilian() : base(new ReptilianFactory())
        {
        }
    }
}
