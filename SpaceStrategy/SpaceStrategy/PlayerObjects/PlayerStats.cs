﻿using SpaceStrategy.Game.GameBoard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game.GameModels;
using SpaceStrategy.Game.GameModels.Ships;

namespace SpaceStrategy.PlayerObjects
{
    public class PlayerStats
    {
        public LinkedList<Player> Enemies { get; set; }
        public LinkedList<Player> Allies { get; set; }
        public LinkedList<Sector> CapturedSectors { get; set; }
        public LinkedList<Fleet> Fleets { get; set; }
        public LinkedList<Ship> Ships { get; set; }

        public PlayerStats()
        {
            Enemies = new LinkedList<Player>();
            Allies = new LinkedList<Player>();
            CapturedSectors = new LinkedList<Sector>();
            Fleets = new LinkedList<Fleet>();
            Ships = new LinkedList<Ship>();
        }
    }
}
