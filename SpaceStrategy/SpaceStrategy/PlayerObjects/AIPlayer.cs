﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.PlayerObjects.Species;

namespace SpaceStrategy.PlayerObjects
{
    public enum Difficulty
    {
        EASY, MEDIUM, HARD, EXTREME
    }
    public class AIPlayer : Player
    {
        public Difficulty Difficulty { get; set; }

        public AIPlayer()
        {
            Difficulty = Difficulty.EASY;
            addRace(new Avian());
        }

        public AIPlayer(string name) : base(name)
        {
            Difficulty = Difficulty.EASY;
            addRace(new Avian());
        }
    }
}
