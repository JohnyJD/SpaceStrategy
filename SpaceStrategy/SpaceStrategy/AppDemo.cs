﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceStrategy.Game;
using SpaceStrategy.Game.GameBoard;
using SpaceStrategy.GameCommands;
using SpaceStrategy.GameCommands.Commands.SectorCommands;
using SpaceStrategy.PlayerObjects;

namespace SpaceStrategy
{
    public class AppDemo
    {
        private App _app;
        private ConsoleDemo _demo;
        public Player Player { get; set; }

        public AppDemo()
        {
            _app = App.GetInstance();
            _demo = new ConsoleDemo();
        }

        public void init()
        {
            string name = _demo.init();
            Player player = new Player(name);
            player.IsHuman = true;
            Player = player;
            _app.init(player);
            
        }

        public void start()
        {
            bool end = false;
            while (!end)
            {
                string action = _demo.menu();
                end = chooseAction(action);

            }
        }

        public bool chooseAction(string action)
        {
            bool endApp = false;
            switch (action)
            {
                case "1":
                    createGalaxy();
                    break;
                case "2":
                    chooseAlienRace();
                    break;
                case "3":
                    addPlayers();
                    break;
                case "4":
                    startGame();
                    break;
                case "5":
                    endApp = true;
                    break;
            }

            return endApp;
        }

        private void startGame()
        {
            if (checkGameRequirements())
            {
                _demo.clear();
                _app.startGame();
                Sector sector = _app.GameController.Game.GameBoard.GetSectors()[0];
                Player.CurrentSector = sector;
                sector.Players.AddLast(Player);
                ControlCommand cmd = new CaptureCommand(Player, sector);
                cmd.execute();
                Player.CurrentSector = sector.NearSectors.ToList()[0];
                sector.notify();
                _app.endGame();
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Hra nie je pripravena. Nezabudnite vytvorit hru, po pripade pridat hracov.");
            }
        }

        private bool checkGameRequirements()
        {
            return _app.isGameReady();
        }

        private void addPlayers()
        {
            Player[] players = _demo.createPlayers();
            foreach (Player player in players)
            {
                _app.addPlayer(player);
            }
        }

        private void chooseAlienRace()
        {
            AlienRace race = _demo.chooseRace();
            Player me = _app.getPlayer(Player.PlayerName);
            if (me != null)
            {
                me.Race = race;
            }
        }

        private void createGalaxy()
        {
            GalaxySize size = _demo.chooseGalaxySize();
            GameSettings.getInstance().GalaxySize = size;
            _app.createGame();
            _demo.clear();
            Console.WriteLine(_app.GameController.Game.GameBoard.toString());
            Console.ReadLine();
        }
    }
}
