﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.GameObserver
{
    public interface Observer
    {
        public void update(Observable observable);
        public void update(Observable observable, Object param);
    }
}
