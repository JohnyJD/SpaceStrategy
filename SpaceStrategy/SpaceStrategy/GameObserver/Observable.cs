﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceStrategy.GameObserver
{
    public interface Observable
    {
        public void attach(Observer observer);
        public void detach(Observer observer);
        public void notify();
    }
}
